"""Zoho target class."""

from __future__ import annotations

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_zoho.sinks import ZohoSink


class TargetZoho(Target):
    """Sample target for Zoho."""

    name = "target-zoho"
    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True)
    ).to_dict()

    default_sink_class = ZohoSink


if __name__ == "__main__":
    TargetZoho.cli()
