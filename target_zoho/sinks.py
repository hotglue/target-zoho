"""Zoho target sink class, which handles writing streams."""

from __future__ import annotations

import re
import logging

import requests
from singer_sdk.helpers._util import utc_now
from singer_sdk.plugin_base import PluginBase
from singer_sdk.sinks import RecordSink
import backoff

from target_zoho.mapping import UnifiedMapping


class Authenticator:
    def __init__(self, config, logger, auth_endpoint) -> None:
        self.config = config
        self.auth_endpoint = auth_endpoint
        self.expires_in = 3600
        self.logger = logger

    @property
    def auth_headers(self) -> dict:
        if not self.is_token_valid():
            self.update_access_token()
        result = {}
        result["Authorization"] = f"Bearer {self.config['access_token']}"
        return result

    def is_token_valid(self) -> bool:
        if self.config.get("last_refreshed") is None:
            return False
        if self.config.get("expires_in", self.expires_in) > (utc_now() - self.config.get("last_refreshed")).total_seconds():
            return True
        return False

    @property
    def oauth_request_body(self) -> dict:
        return {}

    def update_access_token(self) -> None:

        request_time = utc_now()
        token_response = requests.post(
            self.auth_endpoint,
            params={
                "refresh_token": self.config.get("refresh_token"),
                "client_id": self.config.get("client_id"),
                "client_secret": self.config.get("client_secret"),
                "grant_type": "refresh_token",
            },
        )
        self.logger.info(f"Got OAuth refresh response: {token_response.text}")
        try:
            token_response.raise_for_status()
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            raise RuntimeError(
                f"Failed OAuth login, response was '{token_response.json()}'. {ex}"
            )
        token_json = token_response.json()
        self.config["access_token"] = token_json["access_token"]
        self.config["expires_in"] = token_json.get("expires_in", self.expires_in)
        self.config["last_refreshed"] = request_time


class ZohoSink(RecordSink):
    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: dict,
        key_properties: list[str] | None,
    ) -> None:
        super().__init__(target, stream_name, schema, key_properties)
        self.target_name = "zoho"
        self._target = target

    @property
    def auth(self) -> Authenticator:
        auth_endpoint = f"https://accounts.zoho.com/oauth/v2/token"
        return Authenticator(self._target._config, self.logger, auth_endpoint)

    def _get_possible_status(self, status_name, headers):
        response = requests.get("https://www.zohoapis.com/crm/v5/settings/fields?module=Deals", headers=headers)
        if response.status_code != 200:
            self.logger.info("Failed fetching possible statuses for deals")
        
        stage_field = None
        for field in response.json()["fields"]:
            if field["api_name"] == "Stage":
                stage_field = field
        
        if stage_field is None:
            self.logger.info("Failed fetching possible statuses for deals")
        
        for picklist_value in stage_field["pick_list_values"]:
            if "".join(re.findall("\w", picklist_value["forecast_type"])).lower() == status_name:
                return picklist_value["actual_value"]
        
        return None

    def calls_upload(self, record):
        # Format the data
        payload = {
            "data": [
                {
                    "Who_Id": {"id": record.get("contact_id")},
                    "Description": record.get("recording_url"),
                    "Call_Start_Time": record.get("activity_datetime"),
                    "Subject": record.get("title"),
                    "Call_Type": "Outbound",
                    "Outbound_Call_Status": "Completed",
                    "Call_Duration": record.get("duration_seconds")
                }
            ]
        }

        # Send the records
        headers = self.auth.auth_headers
        url = f"https://www.zohoapis.com/crm/v3/Calls"
        self.logger.debug(f"REQUEST - 'url': {url} 'body': {payload}")
        response = requests.post(url, headers=headers, json=payload)

        if response.status_code in [200, 201]:
            self.logger.info("Record uploaded successful!")
        else:
            raise Exception(
                f"Status Code: {response.status_code}\nMessage: {response.text}"
            )

    def contacts_upload(self, record):
        # Format the data
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(record, "contacts", self.target_name)
        headers = self.auth.auth_headers
        contact_id = None

        # get contact_id by mail
        contact_url = f"https://www.zohoapis.com/crm/v5/Contacts/search?email={record.get('emailAddress')}"
        self.logger.debug(f"Looking contact_id by mail")
        self.logger.debug(f"REQUEST - 'url': {contact_url}")
        contact_response = requests.get(contact_url, headers=headers, json=payload)
        
        if contact_response.status_code == 200:
            contact_id = contact_response.json()["data"][0]["id"]

        # if contact mail already exists update the contact
        if contact_id is not None:
            url = f"https://www.zohoapis.com/crm/v3/Contacts/{contact_id}"
            self.logger.debug(f"REQUEST - 'url': {url} 'body': {payload}")
            response = requests.put(url, headers=headers, json=payload)
       
        #if contact mail doesn't exist create the contact
        else:
            url = f"https://www.zohoapis.com/crm/v3/Contacts/upsert"
            self.logger.debug(f"REQUEST - 'url': {url} 'body': {payload}")
            response = requests.post(url, headers=headers, json=payload)            
            
        if response.status_code in [200, 201]:
            self.logger.info("Record uploaded successful!")
        else:
            raise Exception(
                f"Status Code: {response.status_code}\nMessage: {response.text}"
            )
    
    def process_deal(self,record):
        headers = self.auth.auth_headers
        if self._get_possible_status(record["status"], headers) is not None:
            record["status"] = self._get_possible_status(record["status"], headers)
        
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(record, "deals", self.target_name)

        ## get contact_id by mail
        contact_url = f"https://www.zohoapis.com/crm/v5/Contacts/search?email={record.get('contact_email')}"
        self.logger.debug(f"Looking contact_id by mail")
        self.logger.debug(f"REQUEST - 'url': {contact_url}")        

        @backoff.on_exception(backoff.expo, requests.exceptions.RequestException, max_tries=3)
        def get_contact_response(contact_url, headers):
            contact_response = requests.get(contact_url, headers=headers)
            if contact_response.status_code == 200:
                contact = contact_response.json()["data"][0]["id"]
                return contact
            if contact_response.status_code == 204:
                raise requests.exceptions.RequestException

        try:
            contact_id = get_contact_response(contact_url, headers)
        except:
            contact_id = None

        if contact_id is not None:
            payload["data"][0]["Contact_Name"] = contact_id

        # Send the records
        url = f"https://www.zohoapis.com/crm/v3/Deals/upsert"
        self.logger.debug(f"REQUEST - 'url': {url} 'body': {payload}")
        response = requests.post(url, headers=headers, json=payload)
        if response.status_code in [200, 201]:
            self.logger.info("Record uploaded successful!")
        else:
            raise Exception(
                f"Status Code: {response.status_code}\nMessage: {response.text}"
            )
    
    def fallback_sink_upload(self, record):
        if not "data" in record:
            record = {"data": [record]}
        
        # Send the records
        headers = self.auth.auth_headers
        url = f"https://www.zohoapis.com/crm/v3/{self.stream_name}/upsert"
        self.logger.debug(f"REQUEST - 'url': {url} 'body': {record}")
        response = requests.post(url, headers=headers, json=record)
        if response.status_code in [200, 201]:
            self.logger.info("Record uploaded successfully!")
        else:
            raise Exception(
                f"Status Code: {response.status_code}\nMessage: {response.text}"
            )

    def process_record(self, record: dict, context: dict) -> None:
        if self.config.get("fallback_sink") is True:
            self.fallback_sink_upload(record)
        elif self.stream_name == "Activities":
            if record.get("type") == "call":
                self.calls_upload(record)
        elif self.stream_name in ["Customers", "Contacts"]:
            self.contacts_upload(record)
        elif self.stream_name in ["Opportunities", "Deals"]:
            self.process_deal(record)
