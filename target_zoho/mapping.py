import json
import os

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


class UnifiedMapping:
    def __init__(self) -> None:
        pass

    def read_json_file(self, filename):
        # read file
        with open(os.path.join(__location__, f"{filename}"), "r") as filetoread:
            data = filetoread.read()

        # parse file
        content = json.loads(data)

        return content

    # Unified Schema Addresses -> Zoho Addresses
    def map_address(self, address, address_mapping, payload):
        if isinstance(address, str):
            address = json.loads(address)
        if isinstance(address, list):
            # The payload only supports
            for i in range(0, len(address) if len(address) <= 2 else 2):
                for key, value in address[i].items():
                    if key in address_mapping[i].keys():
                        payload[address_mapping[i][key]] = value
        return payload

    def map_phoneNumbers(self, phones, phones_mapping, payload):
        if isinstance(phones, str):
            phones = json.loads(phones)

        if isinstance(phones, list):
            for phone in phones:
                payload[phone["type"].capitalize()] = phone["number"]

        return payload

    def prepare_payload(self, record, endpoint="contacts", target="zoho"):
        mapping = self.read_json_file(f"mapping_{target}.json")
        ignore = mapping["ignore"]
        mapping = mapping[endpoint]
        payload = {}
        payload_return = {}
        lookup_keys = mapping.keys()
        for lookup_key in lookup_keys:
            if lookup_key == "addresses":
                payload = self.map_address(
                    record.get(lookup_key, []), mapping[lookup_key], payload
                )
            elif lookup_key == "phone_numbers":
                payload = self.map_phoneNumbers(
                    record.get(lookup_key, []), mapping[lookup_key], payload
                )
            elif "date" in lookup_key.lower():
                val = record.get(lookup_key)
                if val:
                    payload[mapping[lookup_key]] = val.split("T")[0]
                else:
                    val = ""
            elif lookup_key in ["custom_fields","lists"]:
                #Ignore custom fields for now.
                pass        
            else:
                val = record.get(lookup_key, "")
                if val:
                    payload[mapping[lookup_key]] = val

        for key in payload.keys():
            if key not in ignore and key is not None:
                payload_return[key] = payload[key]
        return {"data": [payload_return]}
